/*
===========================
	Toggle button js
===========================
*/
function toggleDown() {
  var x = document.getElementById("down-content");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
  
  //console.log(x);
}

function toggleLeft() {
  var x = document.getElementById("left-content");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggleRight() {
  var x = document.getElementById("right-content");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggleUp() {
  var x = document.getElementById("up-content");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

/*
===========================
	Animated button js
===========================
*/

function progress() {
  var elem = document.getElementById("progress");   
  var width = 1;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}